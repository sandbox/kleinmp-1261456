<?php

/**
 * Implements hook_content_migrate_field_alter().
 */
function emfield_content_migrate_field_alter(&$field_value, $instance_value) {
  if (in_array($field_value['module'], array('emfield', 'emvideo', 'emimage', 'emaudio'))) {
    $field_value['module'] = 'media';
    $field_value['type'] = 'media';
    // TODO: need to convert settings etc for this field to the D7 version.
  }
}

/**
 * Implements hook_content_migrate_instance_alter().
 */
function emfield_content_migrate_instance_alter(&$instance_value, $field_value) {
  if (in_array($instance_value['widget']['module'], array('emfield', 'emvideo', 'emimage', 'emaudio'))) {
    $instance_value['widget']['module'] = 'media';
    $instance_value['widget']['type'] = 'media_generic';
    foreach ($instance_value['display'] as $view_mode => $display_settings) {
      $instance_value['display'][$view_mode]['type'] = 'media';
      $instance_value['display'][$view_mode]['module'] = 'media';
      $instance_value['display'][$view_mode]['settings'] = array(
        'file_view_mode' => 'media_large',
      );
    }
    // TODO: need to convert settings etc to the D7 version.
  }
}

/**
 * Implements hook_content_migrate_data_record_alter().
 */
function emfield_content_migrate_data_record_alter(&$record, $field, $instance) {
  $providers = array();
  if (module_exists('media_youtube')) {
    $providers['youtube'] = 'youtube://v/';
  }
  if (!empty($record[$field['field_name'] . '_data'])) {
    $data = unserialize($record[$field['field_name'] . '_data']);
    if (!empty($data)) {
      if ($scheme = $providers[$record[$field['field_name'] . '_provider']]) {
        $file = file_uri_to_object($scheme . $record[$field['field_name'] . '_value']);
        file_save($file);
        $record[$field['field_name'] . '_fid'] = $file->fid;
      }
    }
  }
}

/**
 * This will audit all nodes to ensure all emfield providers are present.
 *
 * The function will pass through all emfield fields to see if there is any
 * content in the system without a provider.
 *
 * @return array
 *  An array of all missing providers. Ideally the array will be empty.
 */
function emfield_audit() {
  $audit = array();

  // Build a list of fields that need data updating.
  $fields = array();
  foreach (content_fields() as $field) {
    if (in_array($field['module'], array('emvideo', 'emimage', 'emaudio'))) {
      // We only process a given field once.
      $fields[$field['field_name']] = $field;
    }
  }

  // Update database storage.
  foreach ($fields as $field) {
    $db_info = content_database_info($field);
    $table = $db_info['table'];
    $provider_column = $db_info['columns']['provider']['column'];
    $module = $field['module'];
    $sql = "SELECT $provider_column FROM {{$table}} WHERE $provider_column IS NOT NULL GROUP BY $provider_column";
    $results = db_query($sql)->execute();
    foreach ($results as $result) {
      $provider = emfield_system_list($module, $result->$provider_column);
      if (empty($provider)) {
        $audit[$result->$provider_column] = $result->$provider_column;
      }
    }
  }
  return $audit;
}

/**
 * Return an array of installed .inc files and/or loads them upon request.
 * This routine is modeled after drupal_system_listing() (and also depends on
 * it). It's major difference, however, is that it loads .inc files by default.
 *
 *  @param $provider
 *   (Optional) Name of the passed $provider to find (e.g. "youtube", "google").
 *  @return
 *   An array of file objects optionally matching $provider.
 */
function emfield_system_list($module, $provider = NULL, $options = array()) {
  $files = $override_files = module_invoke_all('emfield_providers', $module, $provider);
//   $files = drupal_system_listing("$provider\.inc$", drupal_get_path('module', $module) ."/providers", 'name', 0);
//   $files = array_merge($files, $override_files);

  ksort($files);

  foreach ($files as $provider => $file) {
    emfield_include_list($file);
    if (!function_exists($module .'_'. $provider .'_info')) {
      emfield_include_list($file, TRUE);
      unset($files[$provider]);
      if (function_exists($module .'_'. $provider .'_obsolete') && variable_get('emfield_'. $module .'_allow_'. $provider, TRUE)) {
        if (isset($options['suppress_errors']) && $options['suppress_errors']) {
          continue;
        }
        $error = t('Attempted to load obsolete provider: %provider at @file.', array('%provider' => $provider, '@file' => $file->filename));
        emfield_set_error($error);
        if (user_access('administer site configuration')) {
          drupal_set_message($error, error);
          call_user_func($module .'_'. $provider .'_obsolete');
          drupal_set_message(t('You may also choose to !turn_off this warning about the missing or obsolete Embedded Media Field %provider provider file.', array('%provider' => $provider, '!turn_off' => l(t('turn off'), 'admin/content/emfield/obsolete/'. $module .'/'. $provider, array('query' => 'destination='. $_GET['q'])))), 'error');
        }
      }
    }
  }

  return $files;
}

/**
 * Maintains a list of all loaded include files.
 *
 * @param $file
 *   Optional; a file object (from emfield_system_list()) to be included.
 * @param $remove
 *   Optional boolean; if TRUE, then remove the file from the list.
 * @return
 *   An array of all loaded includes (without the .inc extension).
 */
function emfield_include_list($file = NULL, $remove = FALSE) {
  static $list = array();

  if ($file && $file->filename && !isset($list[$file->filename])) {
    include_once('./'. $file->filename);
    $list[$file->filename] = $file->name;
  }
  if ($file && $remove) {
    unset($list[$file->filename]);
  }

  return $list;
}


function emfield_set_error($error) {
  watchdog('emfield', '!error', array('!error' => $error), WATCHDOG_WARNING);
}

